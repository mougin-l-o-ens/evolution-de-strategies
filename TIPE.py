#################### Importation ##############################################

import random as rdm
import matplotlib.pyplot as plt
import numpy as np

###################### Construction des strategies ############################

def creation_strat(po):
    s = []
    for k in range(po):
        s.append([])
        for j in range(po):
            if j < k:
                s[k].append(1-s[j][k])
            elif j == k:
                s[k].append(0.5)
            else:
                s[k].append(rdm.betavariate(2,2))
    return s

####################### Mise en place des "joueurs" ###########################

def creation_jeu(jo,po):
    jeu = []
    for i in range(po):
        jeu.append(jo)
    return jeu

######################### Calcul des gains ####################################

def partie(jeu,st,jo):
    gain = []
    po = len(st)
    for i in range(po):
        gi = 0
        if jeu[i] != 0:
            for j in range(len(jeu)):
                if j == i:
                    gi = gi + (jeu[j]-1)*0.5 # en cas de miroir
                else:
                    gi = gi + jeu[j]*st[i][j] # on ajoute au gain partiel le produit du nbe de joueur de la strat j et du taux de victoire
        gain.append([gi/(po*jo-1),i+1]) # on calcule en fait une moyenne
    return gain

######################## repartition des strategies ###########################

##### fonction tri et positivive #######

def tri(sco):
    l = len(sco)
    for i in range(l):
        for j in range(l-1):
            if sco[j] < sco[j+1]:
                aux = sco[j+1]
                sco[j+1] = sco[j]
                sco[j] = aux
    return sco
    
def pos(x):
    if x < 0:
        return 0
    else:
        return x

#########################################

def evol(jeu,n,g,p,dec,tr): # p facteur de repartition, g le gain, dec le facteur de retrait, tr le facteur de mouvement 
    gsp,j,total = tri(g),jeu,0 # gsp = base de la repartition
    for i in range(len(j)):
        nbe_dec = int(np.exp(-n/dec)*j[i]*tr+1)
        total = total + min(j[i]-1,pos(nbe_dec))
        j[i] = max(1,j[i]-pos(nbe_dec))
    while total > 0:
        pr = gsp[0][1]
        j[pr-1] = j[pr-1] + 1
        total = total -1
        gsp[0][0] = gsp[0][0]*p
        gsp = tri(gsp)
    # on va ainsi ajouter a joueur a la strat pr, puis il y aura "moins de place" en pr
    # ainsi il y a moins de chance de remettre un joueur en pr
    return j

def evol_giga(jeu,n,g,p,dec,tr): # p facteur de repartition, g le gain, dec le facteur de retrait, tr le facteur de mouvement 
    gsp,j,total = tri(g),jeu,0 # gsp = base de la repartition
    for i in range(len(j)):
        nbe_dec = int(np.exp(-n/dec)*j[i]*tr+1)
        total = total + min(j[i]-1,pos(nbe_dec))
        j[i] = max(1,j[i]-pos(nbe_dec))
    while total > 0:
        pr = gsp[0][1]
        j[pr-1] = j[pr-1] + 1 + int(total/10) #si on veut aller plus vite
        total = total -1 - int(total/10) #dans ce cas
        gsp[0][0] = gsp[0][0]*p
        gsp = tri(gsp)
    return j

#################### La fonction finale !!!! ##################################

def thegame(jo,po,n,p,dec,tr):
    s = creation_strat(po)
    print(s)
    jeu = creation_jeu(jo,po)
    print(jeu)
    for i in range(n):    
        g = partie(jeu,s,jo)
        jeu = evol(jeu,i,g,p,dec,tr)
        print(jeu)

#h = thegame(10,5,11,10,0.9,0.1)

############## v2 ###############
def data(jo,po,n,p,dec,tr): # cette version est moins visuel mais nous servira dans la suite
    s = creation_strat(po)
    jeu = creation_jeu(jo,po)
    final = [jeu[:]]
    for i in range(n):    
        g = partie(jeu,s,jo)
        jeu = evol(jeu,i,g,p,dec,tr)
        final.append(jeu[:])
    return final

def data_giga(jo,po,n,p,dec,tr): # cette fois on met le paquet !
    s = creation_strat(po)
    jeu = creation_jeu(jo,po)
    final = [jeu[:]]
    for i in range(n):    
        g = partie(jeu,s,po)
        jeu = evol_giga(jeu,i,g,p,dec,tr)
        final.append(jeu[:])
    return final

t = data(10,10,100,0.99,1,0.01)
#t_giga = data_giga(10**6,3,10**3,0.99,10**5,0.01)
#print(t)

################### gestion des tableaux ######################################

############### la fonction transpose ##############

def trans(ma): # va nous aider a ressortir l'evolution du nbe de joueur
    res = []
    for i in range(len(ma[0])):
        aux = []
        for j in range(len(ma)):
            aux.append(ma[j][i])
        res.append(aux)
    return res

####################################################

def affi1(t): # un affichage en tableau
    new_t = trans(t)
    for i in range(len(t[0])):
        plt.plot(new_t[i])
    plt.show()

affi1(t)
#affi1(t_giga)

def affi2(t): # un affichage en part
    l = len(t)
    l2 = int(np.sqrt(l))
    for j in range(l):
        plt.subplot(l2 +1,l2,j+1)
        plt.pie(t[j])
    plt.show()

#affi2(t)
#affi2(t_giga)

def affi3(t): # un affichage en part condensé
    for i in range(len(t)):
        plt.figure(i+1)
        plt.pie(t[i])
        plt.title(i)
    plt.show()

#affi3(t)

################# test avec facteur variable ##################################

po = 5
s = creation_strat(po)


def fact1(jo,po,n,f,dec,tr):
    plt.figure(1)
#    s = creation_strat(po)
    for j in range(len(f)):
        jeu = creation_jeu(jo,po)
        aux = [jeu[:]]
        plt.subplot(2,2,j+1)
        for i in range(n):    
            g = partie(jeu,s,jo)
            jeu = evol(jeu,i,g,f[j],dec,tr)
            aux.append(jeu[:])
        for t in range(len(aux[0])):
            plt.plot(trans(aux)[t], label = f[j])


#f = [0.8,0.85,0.9,0.95]
#fact1(10,po,10,f,10,0.01)
#plt.show()

def fact2(jo,po,n,f,dec,tr):
    plt.figure(2)
    #s = creation_strat(po)
    for j in range(len(dec)):
        jeu = creation_jeu(jo,po)
        aux = [jeu[:]]
        plt.subplot(2,2,j+1)
        for i in range(n):    
            g = partie(jeu,s,jo)
            jeu = evol(jeu,i,g,f,dec[j],tr)
            aux.append(jeu[:])
        for t in range(len(aux[0])):
            plt.plot(trans(aux)[t], label = dec[j])

#dec = [0.1,1,10,100]
#fact2(10,po,10,0.98,dec,0.01)
#plt.show()


def fact3(jo,po,n,f,dec,tr):
    plt.figure(3)
    #s = creation_strat(po)
    for j in range(len(tr)):
        jeu = creation_jeu(jo,po)
        aux = [jeu[:]]
        plt.subplot(2,2,j+1)
        for i in range(n):    
            g = partie(jeu,s,jo)
            jeu = evol(jeu,i,g,f,dec,tr[j])
            aux.append(jeu[:])
        for t in range(len(aux[0])):
            plt.plot(trans(aux)[t], label = tr[j])

#tr = [0,0.1,0.01,0.001]
#fact3(10,po,10,0.98,10,tr)
#plt.show()

#################### quand il faut tout changer ###############################

def premier(jeu): # on detecte la strategie la plus forte a un instant t
    res,c = 0,0
    for i in range(len(jeu)):
        if jeu[i] > c:
            c = jeu[i]
            res = i
    return res

def retire_strat(strat,s):# ou la strategie s est retire des strategies disponible
    res = []
    for i in range(len(strat)):
#        if i != s:
        aux = []
        for j in range(len(strat[0])):
            if j != s:
                aux.append(strat[i][j])
            else:
                aux.append(0)
        res.append(aux)
    return res

def migration_auto(jeu,s,g): # les joueurs de la strat s doivent migrer vers une nouvelle car celle ci disparait
    res,total,g2 = [],jeu[s],[]
    for i in range(len(jeu)):
        if i != s:
            res.append(jeu[i])
            g2.append(g[i])
        else:
            res.append(0)
    gsp = tri(g2)
    while total > 0:
        pr = gsp[0][1] #if gsp[0][1] < s else gsp[0][1]-1
        res[pr-1] = res[pr-1] + 1
        total = total -1
        gsp[0][0] = gsp[0][0]*0.9
        gsp = tri(gsp)
    return res

s = creation_strat(2)
jeu = creation_jeu(10,2)
g = partie(jeu,s,1000)
print(g)
jeu = migration_auto(jeu,0,g)
final = []
final.append(jeu[:])
print(final)

###### petite variation ( ban ) #######

def data_ban(jo,po,n,n_int): # on allege les parametres
    s = creation_strat(po)
    jeu = creation_jeu(jo,po)
    final = [jeu[:]]
    i = 0
    while i < n_int:
        g = partie(jeu,s,jo)
        jeu = evol(jeu,i,g,0.99,100,0.01)
        final.append(jeu[:])
        i = i+1
    prems = premier(jeu)
    s = retire_strat(s,prems)
    jeu = migration_auto(jeu,prems,g)
    final.append(jeu[:])
    while i < n:
        g = partie(jeu,s,jo)
        jeu = evol(jeu,i,g,0.99,100,0.01)
        final.append(jeu[:])
        i=i+1
    return final

t_ban = data_ban(10,5,10,5)
affi1(t_ban)

